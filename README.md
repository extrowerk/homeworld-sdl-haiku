# Homeworld SDL Haiku port#

Homeworld (c) 1999-2000 Sierra On-Line, Inc.  All Rights Reserved.
Homeworld is a registered trademark, and Sierra and Sierra Studios are
registered trademarks of Sierra On-Line, Inc.  Relic is a trademark of Relic
Entertainment, Inc.

SDL/Linux port by Ted Cipicchio <ted@thereisnospork.com>

------------------------------------------------------------------------------

(Please refer to the file INSTALL for installation instructions, along with
building instructions if you are using the source package.  It contains some
very important information on getting Homeworld to work.)

---

On September 26, 2003, Relic Entertainment released the source code to
Homeworld, a 3D real-time strategy game set in space.  Homeworld, originally
released in 1999, was dubbed "Game of the Year" by PC Gamer and "Strategy Game
of the Year" by Computer Gaming World.

This a Haiku port of the Homeworld code released by Relic.  It is still in a
very early state, and many of the features (and stability) of the Windows/Linux
version are not yet implemented.

### How do I get set up? ###

#### Summary of set up ####
* You'll need Haiku GCC2h or GCC4 build
* SDL dev package from Haiku Depot
* And a renderer from HaikuDepot

#### Configuration ####
* Clone this repo
* cd into the homeworld-sdl-haiku/Haiku folder in Terminal
* run the **./bootstrap** script
* run the **../configure** script (It is double point!)
* run **make**
* take a small break

#### Dependencies ####
* libsdl_x86_devel (> 1.2.15-7 required)
* mesa_x86_swrast

### Who do I talk to? ###

#### Repo owner or admin ####
* miqlas ( at) gmail ( doot ) com

#### Other community or team contact ####